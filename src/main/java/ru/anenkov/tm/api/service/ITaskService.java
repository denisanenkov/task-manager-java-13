package ru.anenkov.tm.api.service;

import ru.anenkov.tm.model.Task;

import java.util.List;

public interface ITaskService {

    void create(String name);

    void create(String name, String description);

    void add(Task task);

    void remove(Task task);

    List<Task> findAll();

    void clear();

    Task findOneByIndex(Integer index);

    Task findOneByName(String name);

    Task findOneById(String id);

    Task removeOneByIndex(Integer index);

    Task removeOneByName(String name);

    Task removeOneById(String id);

    Task updateTaskById(String id, String name, String description);

    Task updateTaskByIndex(Integer index, String name, String description);

}
