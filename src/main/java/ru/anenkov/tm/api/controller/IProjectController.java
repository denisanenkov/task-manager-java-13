package ru.anenkov.tm.api.controller;

public interface IProjectController {

    void showProjects();

    void clearProjects();

    void createProject();

    void showTaskByIndex();

    void showTaskByName();

    void showTaskById();

    void removeTaskByIndex();

    void removeTaskByName();

    void removeTaskById();

    void updateTaskByIndex();

    void updateTaskById();

}
