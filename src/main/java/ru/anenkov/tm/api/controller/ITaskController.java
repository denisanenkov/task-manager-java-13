package ru.anenkov.tm.api.controller;

import ru.anenkov.tm.model.Task;

public interface ITaskController {

    void showTasks();

    void clearTasks();

    void createTask();

    void showTaskByIndex();

    void showTaskByName();

    void showTaskById();

    void removeTaskByIndex();

    void removeTaskByName();

    void removeTaskById();

    void updateTaskByIndex();

    void updateTaskById();

}
