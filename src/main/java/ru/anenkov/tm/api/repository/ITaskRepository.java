package ru.anenkov.tm.api.repository;

import ru.anenkov.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    void add(Task task);

    void remove(Task task);

    List<Task> findAll();

    void clear();

    Task findOneByIndex(Integer index);

    Task findOneByName(String name);

    Task findOneById(String id);

    Task removeOneByIndex(Integer index);

    Task removeOneByName(String name);

    Task removeOneById(String id);

}
