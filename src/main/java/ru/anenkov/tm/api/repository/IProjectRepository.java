package ru.anenkov.tm.api.repository;

import ru.anenkov.tm.model.Project;
import ru.anenkov.tm.model.Task;

import java.util.List;

public interface IProjectRepository {

    void add(Project project);

    void remove(Project project);

    List<Project> findAll();

    void clear();

    Project findOneByIndex(Integer index);

    Project findOneByName(String name);

    Project findOneById(String id);

    Project removeOneByIndex(Integer index);

    Project removeOneByName(String name);

    Project removeOneById(String id);

}
