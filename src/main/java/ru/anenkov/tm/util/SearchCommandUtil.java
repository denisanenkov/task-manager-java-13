package ru.anenkov.tm.util;

import ru.anenkov.tm.repository.CommandRepository;

public interface SearchCommandUtil {

    static boolean commandFind(String arg) {
        final CommandRepository commandRepository = new CommandRepository();
        final String[] commands = commandRepository.getCommands();
        for (String command : commands) {
            if (command.equals(arg)) return true;
        }
        return false;
    }

}
