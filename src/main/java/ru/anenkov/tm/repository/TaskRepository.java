package ru.anenkov.tm.repository;

import ru.anenkov.tm.api.repository.ITaskRepository;
import ru.anenkov.tm.error.IdEmptyException;
import ru.anenkov.tm.error.IndexIncorrectException;
import ru.anenkov.tm.error.NameEmptyException;
import ru.anenkov.tm.model.Task;

import java.util.ArrayList;
import java.util.List;

public class TaskRepository implements ITaskRepository {

    private final List<Task> tasks = new ArrayList<>();

    @Override
    public void add(final Task task) {
        if (task == null) return;
        tasks.add(task);
    }

    @Override
    public List<Task> findAll() {
        return tasks;
    }

    @Override
    public void clear() {
        tasks.clear();
    }

    @Override
    public void remove(final Task task) {
        if (task == null) return;
        tasks.remove(task);
    }

    @Override
    public Task findOneByIndex(final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        return tasks.get(index);
    }

    @Override
    public Task findOneByName(final String name) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        for (final Task task : tasks) {
            if (name.equals(task.getName())) return task;
        }
        return null;
    }

    @Override
    public Task removeOneByIndex(final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        final Task task = findOneByIndex(index);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

    @Override
    public Task removeOneByName(final String name) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final Task task = findOneByName(name);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

    @Override
    public Task findOneById(final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        for (final Task task : tasks) {
            if (id.equals(task.getId())) return task;
        }
        return null;
    }

    @Override
    public Task removeOneById(final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        final Task task = findOneById(id);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

}
