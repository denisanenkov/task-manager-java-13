package ru.anenkov.tm.constant;

public interface LiteralConst {

    String HELP = "Display terminal commands.";

    String VERSION = "Show version info.";

    String ABOUT = "Show developer info.";

    String EXIT = "Close application.";

    String INFO = "Display information about system.";

    String ARGUMENTS = "Show program arguments.";

    String COMMANDS = "Show program commands.";

    String TASK_CREATE = "Create new task";

    String TASK_CLEAR = "Remove all tasks";

    String TASK_LIST = "Show task list";

    String PROJECT_CREATE = "Create new project";

    String PROJECT_CLEAR = "Remove all projects";

    String PROJECT_LIST = "Show all projects";

    String TASK_UPDATE_BY_INDEX = "Update task - list by index";

    String TASK_UPDATE_BY_ID = "Update task - list by id";

    String TASK_VIEW_BY_ID = "Show task - list by id";

    String TASK_VIEW_BY_INDEX = "Show task - list by index";

    String TASK_VIEW_BY_NAME = "Show task - list by name";

    String TASK_REMOVE_BY_ID = "Delete task from task - list by id";

    String TASK_REMOVE_BY_INDEX = "Delete task from task - list by index";

    String TASK_REMOVE_BY_NAME = "Delete task from task - list by name";

    String PROJECT_UPDATE_BY_INDEX = "Update project - list by index";

    String PROJECT_UPDATE_BY_ID = "Update project - list by id";

    String PROJECT_VIEW_BY_ID = "Show project - list by id";

    String PROJECT_VIEW_BY_INDEX = "Show project - list by index";

    String PROJECT_VIEW_BY_NAME = "Show project - list by name";

    String PROJECT_REMOVE_BY_ID = "Delete project from task - list by id";

    String PROJECT_REMOVE_BY_INDEX = "Delete project from task - list by index";

    String PROJECT_REMOVE_BY_NAME = "Delete project from task - list by name";

}
