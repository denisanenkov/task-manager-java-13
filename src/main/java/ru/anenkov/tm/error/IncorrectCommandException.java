package ru.anenkov.tm.error;

public class IncorrectCommandException extends RuntimeException {

    public IncorrectCommandException(final String command) {
        super("Error! ``" + command + "`` is not a command! Re-enter command!");
    }

    public IncorrectCommandException(Throwable cause) {
        super(cause);
    }

}
